<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use App\User;

class NotificationController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function VerifyEmal($value='')
    {
    	$user = Auth::User();
    	UserVerification::generate($user);
		UserVerification::send($user, 'My Custom E-mail Subject');
		return 'ok';
    }
    public function getVerification(Request $request, $token)
    {
    	$email = $request->input('email');
    	$user = User::where('email', $email)->first();

    	if(!$user){
    		return 'user not found';
    	}
    	if($token != $user->verification_token){
    		return 'not match token';
    	}

    	$user->verification_token = null;
    	$user->verified = 1;
    	$user->save();
    	return 'ok';
    }
}
