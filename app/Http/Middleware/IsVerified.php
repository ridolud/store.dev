<?php

namespace App\Http\Middleware;

use Closure;
use Jrean\UserVerification\Exceptions\UserNotVerifiedException;
use Alert;

class IsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if(! is_null($request->user()) && ! $request->user()->verified) {
            // throw new UserNotVerifiedException;
            Alert::info('Verify your email. <a href="'. url('/verify_email') .'">Lest verify</a>');
        }

        return $response;
    }
}
