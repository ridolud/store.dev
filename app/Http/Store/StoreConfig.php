<?php 
namespace App\Http\Store;

use View;
use Request;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StoreConfig
{
	public static function check()
	{

		try {

			$subdomain = explode('.', Request::getHost())[0];

			$tmp = User::where('store_name', $subdomain)->firstOrFail();

			View::addNamespace('Store', realpath(base_path('resources/views/store')));

			return $tmp;

		} catch (ModelNotFoundException $e) {

			abort(404, 'Subdomain not found.');

		}

	}
}