<?php
namespace App\Http\Store;

use View;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	//
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
    	return View::make('Store::home');
        // dd(Auth::guard('store')->user());
    }

}