<?php

namespace App\Http\Store;

use View;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;

class AccountController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    
    protected function guard()
    {
        return Auth::guard('store');
        // return Auth::guard();
    }

    protected function passwords()
    {
        return Auth::passwords('customers');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('store', ['except' => 'logout']);
    }

    public function loginForm()
    {
        return View::make('Store::login');
    }

    public function registerForm()
    {
        return View::make('Store::register');
    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if($this->guard()->attempt($credentials)){
           return redirect($this->redirectTo); 
        }
        return 'gagal';
    }

    public function logout()
    {
        if($this->guard()->logout())
        {
            Session::flush();
            return redirect($this->redirectTo);
        }
        return back();

    }


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered(
            $customer = Customer::create([
                    'first_name'    => $request->first_name,
                    'last_name'    => $request->last_name,
                    'email'         => $request->email,
                    'password'      => bcrypt($request->password),
                    'store_id'      =>  app('getStoreData')['store_id'],
                ])
            ));

        $this->guard()->attempt(['email' => $customer->email, 'password' => $request->password]);

        return $this->registered($request, $customer)
                        ?: redirect($this->redirectTo);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'email' => 'required|email|max:255|unique:customers',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function registered(Request $request, $user)
    {
        //return redirect('/account');
    }

}
