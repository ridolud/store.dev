<?php

namespace App\Providers;

// use Store\Connection;
use Illuminate\Support\ServiceProvider;

use Config;
use Request;
use App\Http\Store\StoreConfig;
use Log;

class StoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ( Request::getHost() !== env('APP_DOMAIN') && Request::getHost() !== 'localhost'){
                        
            $config = StoreConfig::check();
            
            $this->app->singleton('getStoreData', function() use ($config) {
                return [ 
                    'store_name' => $config->store_name,
                    'store_id' => $config->store_id,
                    'store_owner' => $config->name,
                ];
            });

            view()->share('store_data', $config);

        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->singleton(Connection::class, function ($app) {
        //     return new Connection(config('store'));
        // });
    }
}
