<?php

/*
|--------------------------------------------------------------------------
| Store Routes
|--------------------------------------------------------------------------
|
| This route for store user
|
*/

Route::get('/', 'AppController@index');
Route::get('/home', 'AppController@index');

// Account
Route::get('/login', 'AccountController@loginForm');
Route::post('/login', 'AccountController@login');
Route::get('/register','AccountController@registerForm');
Route::post('/register','AccountController@register');
Route::post('/logout','AccountController@logout');

// Must Login
Route::group(['middleware' => 'auth:store'], function(){
	Route::group(['prefix' => 'account'],function(){
		Route::get('/', function (){return 'This profil\'s page';})->name('profile');
	});
});

